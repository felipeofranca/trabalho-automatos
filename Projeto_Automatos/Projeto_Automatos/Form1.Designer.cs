﻿namespace Projeto_Automatos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_add_Produção = new System.Windows.Forms.Button();
            this.lblProducaoTotal = new System.Windows.Forms.Label();
            this.txt_Producao = new System.Windows.Forms.TextBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.txtvarivel = new System.Windows.Forms.TextBox();
            this.lblvariavel = new System.Windows.Forms.Label();
            this.lblproducao = new System.Windows.Forms.Label();
            this.btn_simplificar = new System.Windows.Forms.Button();
            this.btn_terminais = new System.Windows.Forms.Button();
            this.lbl_terminais = new System.Windows.Forms.Label();
            this.btn_adicionar_Gramatica = new System.Windows.Forms.Button();
            this.txt_terminais = new System.Windows.Forms.TextBox();
            this.lbl_obs = new System.Windows.Forms.Label();
            this.lbl_obs_producoes = new System.Windows.Forms.Label();
            this.txt_variavel_gramatica = new System.Windows.Forms.TextBox();
            this.lbl_variavel_gramatica = new System.Windows.Forms.Label();
            this.lbl_gramatica = new System.Windows.Forms.Label();
            this.lbl_inicial = new System.Windows.Forms.Label();
            this.txt_inicial = new System.Windows.Forms.TextBox();
            this.lbl_obs_2 = new System.Windows.Forms.Label();
            this.lbl_gramatica_simplificacao = new System.Windows.Forms.Label();
            this.lbl_producoes_simplificacao = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btn_add_Produção
            // 
            this.Btn_add_Produção.Location = new System.Drawing.Point(31, 87);
            this.Btn_add_Produção.Name = "Btn_add_Produção";
            this.Btn_add_Produção.Size = new System.Drawing.Size(109, 23);
            this.Btn_add_Produção.TabIndex = 2;
            this.Btn_add_Produção.Text = "Adicionar Produção";
            this.Btn_add_Produção.UseVisualStyleBackColor = true;
            this.Btn_add_Produção.Click += new System.EventHandler(this.Btn_add_Produção_Click);
            // 
            // lblProducaoTotal
            // 
            this.lblProducaoTotal.AutoSize = true;
            this.lblProducaoTotal.Location = new System.Drawing.Point(28, 182);
            this.lblProducaoTotal.Name = "lblProducaoTotal";
            this.lblProducaoTotal.Size = new System.Drawing.Size(64, 13);
            this.lblProducaoTotal.TabIndex = 3;
            this.lblProducaoTotal.Text = "Produções: ";
            // 
            // txt_Producao
            // 
            this.txt_Producao.Location = new System.Drawing.Point(465, 86);
            this.txt_Producao.Name = "txt_Producao";
            this.txt_Producao.Size = new System.Drawing.Size(100, 20);
            this.txt_Producao.TabIndex = 4;
            this.txt_Producao.Visible = false;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(465, 139);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(64, 23);
            this.btn_add.TabIndex = 5;
            this.btn_add.Text = "Adicionar";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Visible = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // txtvarivel
            // 
            this.txtvarivel.Location = new System.Drawing.Point(465, 58);
            this.txtvarivel.MaxLength = 1;
            this.txtvarivel.Name = "txtvarivel";
            this.txtvarivel.Size = new System.Drawing.Size(100, 20);
            this.txtvarivel.TabIndex = 6;
            this.txtvarivel.Visible = false;
            // 
            // lblvariavel
            // 
            this.lblvariavel.AutoSize = true;
            this.lblvariavel.Location = new System.Drawing.Point(406, 61);
            this.lblvariavel.Name = "lblvariavel";
            this.lblvariavel.Size = new System.Drawing.Size(45, 13);
            this.lblvariavel.TabIndex = 7;
            this.lblvariavel.Text = "Variavel";
            this.lblvariavel.Visible = false;
            // 
            // lblproducao
            // 
            this.lblproducao.AutoSize = true;
            this.lblproducao.Location = new System.Drawing.Point(406, 89);
            this.lblproducao.Name = "lblproducao";
            this.lblproducao.Size = new System.Drawing.Size(53, 13);
            this.lblproducao.TabIndex = 8;
            this.lblproducao.Text = "Produção";
            this.lblproducao.Visible = false;
            // 
            // btn_simplificar
            // 
            this.btn_simplificar.Location = new System.Drawing.Point(31, 115);
            this.btn_simplificar.Name = "btn_simplificar";
            this.btn_simplificar.Size = new System.Drawing.Size(75, 23);
            this.btn_simplificar.TabIndex = 9;
            this.btn_simplificar.Text = "Simplificar";
            this.btn_simplificar.UseVisualStyleBackColor = true;
            this.btn_simplificar.Click += new System.EventHandler(this.btn_simplificar_Click);
            // 
            // btn_terminais
            // 
            this.btn_terminais.Location = new System.Drawing.Point(31, 58);
            this.btn_terminais.Name = "btn_terminais";
            this.btn_terminais.Size = new System.Drawing.Size(116, 23);
            this.btn_terminais.TabIndex = 10;
            this.btn_terminais.Text = "Adicionar Gramática";
            this.btn_terminais.UseVisualStyleBackColor = true;
            this.btn_terminais.Click += new System.EventHandler(this.btn_terminais_Click);
            // 
            // lbl_terminais
            // 
            this.lbl_terminais.AutoSize = true;
            this.lbl_terminais.Location = new System.Drawing.Point(406, 235);
            this.lbl_terminais.Name = "lbl_terminais";
            this.lbl_terminais.Size = new System.Drawing.Size(52, 13);
            this.lbl_terminais.TabIndex = 11;
            this.lbl_terminais.Text = "Terminais";
            this.lbl_terminais.Visible = false;
            // 
            // btn_adicionar_Gramatica
            // 
            this.btn_adicionar_Gramatica.Location = new System.Drawing.Point(465, 276);
            this.btn_adicionar_Gramatica.Name = "btn_adicionar_Gramatica";
            this.btn_adicionar_Gramatica.Size = new System.Drawing.Size(75, 23);
            this.btn_adicionar_Gramatica.TabIndex = 12;
            this.btn_adicionar_Gramatica.Text = "Adicionar";
            this.btn_adicionar_Gramatica.UseVisualStyleBackColor = true;
            this.btn_adicionar_Gramatica.Visible = false;
            this.btn_adicionar_Gramatica.Click += new System.EventHandler(this.btn_adicionar_gramatica_Click);
            // 
            // txt_terminais
            // 
            this.txt_terminais.Location = new System.Drawing.Point(465, 232);
            this.txt_terminais.Name = "txt_terminais";
            this.txt_terminais.Size = new System.Drawing.Size(100, 20);
            this.txt_terminais.TabIndex = 13;
            this.txt_terminais.Visible = false;
            // 
            // lbl_obs
            // 
            this.lbl_obs.AutoSize = true;
            this.lbl_obs.Location = new System.Drawing.Point(406, 260);
            this.lbl_obs.Name = "lbl_obs";
            this.lbl_obs.Size = new System.Drawing.Size(184, 13);
            this.lbl_obs.TabIndex = 14;
            this.lbl_obs.Text = "Separe os terminais e variaveis por \' ,\'";
            this.lbl_obs.Visible = false;
            // 
            // lbl_obs_producoes
            // 
            this.lbl_obs_producoes.AutoSize = true;
            this.lbl_obs_producoes.Location = new System.Drawing.Point(407, 109);
            this.lbl_obs_producoes.Name = "lbl_obs_producoes";
            this.lbl_obs_producoes.Size = new System.Drawing.Size(141, 13);
            this.lbl_obs_producoes.TabIndex = 15;
            this.lbl_obs_producoes.Text = "Separe as produções por \' | \'";
            this.lbl_obs_producoes.Visible = false;
            // 
            // txt_variavel_gramatica
            // 
            this.txt_variavel_gramatica.Location = new System.Drawing.Point(465, 179);
            this.txt_variavel_gramatica.Name = "txt_variavel_gramatica";
            this.txt_variavel_gramatica.Size = new System.Drawing.Size(100, 20);
            this.txt_variavel_gramatica.TabIndex = 17;
            this.txt_variavel_gramatica.Visible = false;
            // 
            // lbl_variavel_gramatica
            // 
            this.lbl_variavel_gramatica.AutoSize = true;
            this.lbl_variavel_gramatica.Location = new System.Drawing.Point(406, 182);
            this.lbl_variavel_gramatica.Name = "lbl_variavel_gramatica";
            this.lbl_variavel_gramatica.Size = new System.Drawing.Size(50, 13);
            this.lbl_variavel_gramatica.TabIndex = 16;
            this.lbl_variavel_gramatica.Text = "Variáveis";
            this.lbl_variavel_gramatica.Visible = false;
            // 
            // lbl_gramatica
            // 
            this.lbl_gramatica.AutoSize = true;
            this.lbl_gramatica.Location = new System.Drawing.Point(28, 159);
            this.lbl_gramatica.Name = "lbl_gramatica";
            this.lbl_gramatica.Size = new System.Drawing.Size(61, 13);
            this.lbl_gramatica.TabIndex = 18;
            this.lbl_gramatica.Text = "Gramática: ";
            // 
            // lbl_inicial
            // 
            this.lbl_inicial.AutoSize = true;
            this.lbl_inicial.Location = new System.Drawing.Point(406, 209);
            this.lbl_inicial.Name = "lbl_inicial";
            this.lbl_inicial.Size = new System.Drawing.Size(34, 13);
            this.lbl_inicial.TabIndex = 20;
            this.lbl_inicial.Text = "Inicial";
            this.lbl_inicial.Visible = false;
            // 
            // txt_inicial
            // 
            this.txt_inicial.Location = new System.Drawing.Point(465, 206);
            this.txt_inicial.MaxLength = 1;
            this.txt_inicial.Name = "txt_inicial";
            this.txt_inicial.Size = new System.Drawing.Size(100, 20);
            this.txt_inicial.TabIndex = 19;
            this.txt_inicial.Visible = false;
            // 
            // lbl_obs_2
            // 
            this.lbl_obs_2.AutoSize = true;
            this.lbl_obs_2.Location = new System.Drawing.Point(406, 123);
            this.lbl_obs_2.Name = "lbl_obs_2";
            this.lbl_obs_2.Size = new System.Drawing.Size(98, 13);
            this.lbl_obs_2.TabIndex = 21;
            this.lbl_obs_2.Text = "Use \' # \' para vazio";
            this.lbl_obs_2.Visible = false;
            // 
            // lbl_gramatica_simplificacao
            // 
            this.lbl_gramatica_simplificacao.AutoSize = true;
            this.lbl_gramatica_simplificacao.Location = new System.Drawing.Point(25, 318);
            this.lbl_gramatica_simplificacao.Name = "lbl_gramatica_simplificacao";
            this.lbl_gramatica_simplificacao.Size = new System.Drawing.Size(61, 13);
            this.lbl_gramatica_simplificacao.TabIndex = 23;
            this.lbl_gramatica_simplificacao.Text = "Gramática: ";
            // 
            // lbl_producoes_simplificacao
            // 
            this.lbl_producoes_simplificacao.AutoSize = true;
            this.lbl_producoes_simplificacao.Location = new System.Drawing.Point(25, 344);
            this.lbl_producoes_simplificacao.Name = "lbl_producoes_simplificacao";
            this.lbl_producoes_simplificacao.Size = new System.Drawing.Size(64, 13);
            this.lbl_producoes_simplificacao.TabIndex = 22;
            this.lbl_producoes_simplificacao.Text = "Produções: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 296);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Simplificação";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 447);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_gramatica_simplificacao);
            this.Controls.Add(this.lbl_producoes_simplificacao);
            this.Controls.Add(this.lbl_obs_2);
            this.Controls.Add(this.lbl_inicial);
            this.Controls.Add(this.txt_inicial);
            this.Controls.Add(this.lbl_gramatica);
            this.Controls.Add(this.txt_variavel_gramatica);
            this.Controls.Add(this.lbl_variavel_gramatica);
            this.Controls.Add(this.lbl_obs_producoes);
            this.Controls.Add(this.lbl_obs);
            this.Controls.Add(this.txt_terminais);
            this.Controls.Add(this.btn_adicionar_Gramatica);
            this.Controls.Add(this.lbl_terminais);
            this.Controls.Add(this.btn_terminais);
            this.Controls.Add(this.btn_simplificar);
            this.Controls.Add(this.lblproducao);
            this.Controls.Add(this.lblvariavel);
            this.Controls.Add(this.txtvarivel);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.txt_Producao);
            this.Controls.Add(this.lblProducaoTotal);
            this.Controls.Add(this.Btn_add_Produção);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_add_Produção;
        private System.Windows.Forms.Label lblProducaoTotal;
        private System.Windows.Forms.TextBox txt_Producao;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.TextBox txtvarivel;
        private System.Windows.Forms.Label lblvariavel;
        private System.Windows.Forms.Label lblproducao;
        private System.Windows.Forms.Button btn_simplificar;
        private System.Windows.Forms.Button btn_terminais;
        private System.Windows.Forms.Label lbl_terminais;
        private System.Windows.Forms.Button btn_adicionar_Gramatica;
        private System.Windows.Forms.TextBox txt_terminais;
        private System.Windows.Forms.Label lbl_obs;
        private System.Windows.Forms.Label lbl_obs_producoes;
        private System.Windows.Forms.TextBox txt_variavel_gramatica;
        private System.Windows.Forms.Label lbl_variavel_gramatica;
        private System.Windows.Forms.Label lbl_gramatica;
        private System.Windows.Forms.Label lbl_inicial;
        private System.Windows.Forms.TextBox txt_inicial;
        private System.Windows.Forms.Label lbl_obs_2;
        private System.Windows.Forms.Label lbl_gramatica_simplificacao;
        private System.Windows.Forms.Label lbl_producoes_simplificacao;
        private System.Windows.Forms.Label label3;

    }
}

