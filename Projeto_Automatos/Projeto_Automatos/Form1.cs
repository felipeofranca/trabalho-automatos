﻿using Projeto_Automatos.model;
using Projeto_Automatos.MODEL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projeto_Automatos
{
    public partial class Form1 : Form
    {
        Gramatica Gramatica;

        public Form1()
        {
            InitializeComponent();

        }

        private void Btn_add_Produção_Click(object sender, EventArgs e)
        {
            VisibleProducao(true);         
        }

        private void VisibleProducao(bool visible)
        {
            txt_Producao.Visible = visible;
            txtvarivel.Visible = visible;
            lblproducao.Visible = visible;
            lblvariavel.Visible = visible;
            btn_add.Visible = visible;
            lbl_obs_producoes.Visible = visible;
            lbl_obs_2.Visible = visible;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            string producoes = txt_Producao.Text;
            char variavel = 'a';
            if(String.IsNullOrEmpty(txtvarivel.Text))
            {
                MessageBox.Show("Campo variavel vazio");
                limparcampos();
                return;
            }
            variavel = Convert.ToChar(txtvarivel.Text);
            if(producoes == null)
            {
                MessageBox.Show("Campo producao vazio");
                limparcampos();
                return;
            }
            if(Gramatica == null)
            {
                MessageBox.Show("Adicione os terminais e variáveis primeiro");
                limparcampos();
                return;
            }
            var buscavariavel = Gramatica.Producao.FirstOrDefault(f => f.Variavel == variavel);
            if (buscavariavel == null)
            {
                MessageBox.Show("Variável não existe");
                limparcampos();
                return;
            }
            if(buscavariavel.Producoes.Count > 0)
            {
                MessageBox.Show("Já existe Produção");
                limparcampos();
                return;
            }

            List<string> producao = new List<string>();

            if(producoes.Contains('|'))
            {
                producao = producoes.Split('|').ToList();
            }
            else
            {
                producao.Add(producoes);
            }

            if(!VerificaVariavelETerminal(producao))
            {
                limparcampos();
                return;
            }
            Gramatica.Producao.FirstOrDefault(f => f.Variavel == variavel).Producoes = producao;
                        
            string producaotext = string.Empty;

            lblProducaoTotal.Text = String.Empty;

            foreach (var producoestext in Gramatica.Producao)
            {
                lblProducaoTotal.Text += "\n" + producoestext.Variavel + " -> ";  
                for (int i = 0; i < producoestext.Producoes.Count; i++)
                {
                    if(i == 0)
                    lblProducaoTotal.Text += producoestext.Producoes[i];
                    else
                    lblProducaoTotal.Text += "|" + producoestext.Producoes[i];
                }
            }

            for (int i = 0; i < producao.Count; i++)
            {
                if (i == 0)
                    producaotext += producao[i];
                else
                    producaotext += "|" + producao[i];
            }

            limparcampos();

            VisibleProducao(false);        
        }

        public void limparcampos()
        {
            txt_Producao.Text = string.Empty;
            txtvarivel.Text = string.Empty;
        }

        public bool VerificaVariavelETerminal(List<string> producao)
        {
            List<char> variaveis = Gramatica.Producao.Select(s => s.Variavel).ToList();
            bool existeVariavel = false;
            bool temvariavel = false;
            bool existeTerminal = false;
            bool temterminal = false;
            for (int i = 0; i < producao.Count; i++)
            {
                for (int j = 0; j < producao[i].Length; j++)
                {
                    if (char.IsLower(producao[i][j]))
                    {
                        temterminal = true;
                        for (int m = 0; m < Gramatica.Terminais.Count; m++)
                        {
                            if (Convert.ToChar(Gramatica.Terminais[m]) == producao[i][j])
                                existeTerminal = true;
                        }
                    }
                    else
                    {
                        temvariavel = true;
                        for (int m = 0; m < variaveis.Count; m++)
                        {
                            if (Convert.ToChar(variaveis[m]) == producao[i][j])
                                existeVariavel = true;
                        }
                    }

                }
                if (!existeTerminal && temterminal)
                {
                    MessageBox.Show("Não existe terminal");
                    return false;
                }
                else if (!existeVariavel && temvariavel)
                {
                    MessageBox.Show("Não existe variavel");
                    return false;
                }
            }
            return true;
        }

        private void btn_simplificar_Click(object sender, EventArgs e)
        {
            List<string> terminais = new List<string>();
            terminais.Add("a");
            terminais.Add("c");
            Producao p = new Producao('S', new List<string>() { "aAa" });
            Producao p2 = new Producao('A', new List<string>() { "a", "S" });
            Producao p3 = new Producao('C', new List<string>() { "c" });

            Gramatica g = new model.Gramatica(terminais, new List<Producao>() { p, p2, p3 });
            Simplificar s = new Simplificar(g);
            Gramatica = s.Gramatica;
            AddSimplificacaoParaTela();
        }

        private void AddSimplificacaoParaTela()
        {
            string variaveistext = string.Empty;
            string terminaistext = string.Empty;

            for (int i = 0; i < Gramatica.Producao.Count; i++)
            {
                if (i == 0)
                    variaveistext += Gramatica.Producao[i].Variavel;
                else
                    variaveistext += "," + Gramatica.Producao[i].Variavel;
            }

            for (int j = 0; j < Gramatica.Terminais.Count; j++)
            {
                if (j == 0)
                    terminaistext += Gramatica.Terminais[j];
                else
                    terminaistext += "," + Gramatica.Terminais[j];
            }

            lbl_gramatica.Text = lbl_gramatica.Text + "( { " + variaveistext + " } , { " + terminaistext + " } ,P," + Gramatica.Producao[0].Variavel + ")";

            string producaotext = string.Empty;

            foreach (var producoestext in Gramatica.Producao)
            {
                lbl_producoes_simplificacao.Text += "\n" + producoestext.Variavel + " -> ";
                for (int i = 0; i < producoestext.Producoes.Count; i++)
                {
                    if (i == 0)
                        lbl_producoes_simplificacao.Text += producoestext.Producoes[i];
                    else
                        lbl_producoes_simplificacao.Text += "|" + producoestext.Producoes[i];
                }
            }
        }

        private void btn_terminais_Click(object sender, EventArgs e)
        {
            Visiblegramatica(true);          
        }

        private void Visiblegramatica(bool visible)
        {
            lbl_obs.Visible = visible;
            lbl_terminais.Visible = visible;
            txt_terminais.Visible = visible;
            btn_adicionar_Gramatica.Visible = visible;
            txt_variavel_gramatica.Visible = visible;
            lbl_variavel_gramatica.Visible = visible;
            lbl_inicial.Visible = visible;
            txt_inicial.Visible = visible;
        }
               
        private void btn_adicionar_gramatica_Click(object sender, EventArgs e)
        {
            Visiblegramatica(false);

            string terminais = txt_terminais.Text.ToLower();
            txt_terminais.Text = string.Empty;

            string variaveis = txt_variavel_gramatica.Text.ToUpper();
            txt_variavel_gramatica.Text = string.Empty;

            string inicial =  txt_inicial.Text.ToUpper();
            txt_inicial.Text = string.Empty;
            
            List<Producao> p = new List<Producao>();
            p.Add(new Producao(Convert.ToChar(inicial), new List<string>()));

            for (int i = 0; i < variaveis.Length; i++)
            {
                if (variaveis[i] != ',' && variaveis[i] != Convert.ToChar(inicial))
                    p.Add(new Producao(variaveis[i], new List<string>()));
            }
            
            Gramatica = null;
            Gramatica = new Gramatica(terminais.Split(',').ToList(), p);
            string variaveistext = string.Empty;
            string terminaistext = string.Empty;

            for (int i = 0; i < Gramatica.Producao.Count; i++)
            {
                if(i == 0)
                    variaveistext +=Gramatica.Producao[i].Variavel;
                else
                variaveistext += "," + Gramatica.Producao[i].Variavel;
            }

            for (int j = 0; j < Gramatica.Terminais.Count; j++)
            {
                if(j == 0)
                terminaistext +=Gramatica.Terminais[j];
                else
                terminaistext += "," + Gramatica.Terminais[j];
            }

            lbl_gramatica.Text = lbl_gramatica.Text + "( { "+ variaveistext +" } , { " + terminaistext + " } ,P," + inicial + ")";
        }
    }


}
