﻿using Projeto_Automatos.model;
using Projeto_Automatos.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.FNG
{
    public class FNC
    {
        private Gramatica m_gramatica;

        public FNC(Gramatica gramatica)
        {
            m_gramatica = gramatica;
        }

        public void Normalizar()
        {
            EncontraVariavelDireita();

        }

        private void EncontraVariavelDireita()
        {
            foreach (var producoes in m_gramatica.Producao)
            {
                char[] aux = new char[100];

                foreach (string prod in producoes.Producoes)
                {
                    aux = prod.ToCharArray();

                    List<int> terminaisRemover = new List<int>();
                    
                    for (int i = 0; i < prod.Length; i++)
                    {
                        if (char.IsUpper(prod[i]) && char.IsLower(prod[i + 1]))
                        {
                            SubstituirPorVariavel(prod[i]);
                            terminaisRemover.Add(i + 1);
                        }
                    }
                    
                    foreach (var index in terminaisRemover)
                    {
                        aux[index] = ' ';
                    }    
                }
            }
        }

        private void SubstituirPorVariavel(char terminal)
        {
            char var = AddVariavel();

            List<string> producoes = new List<string>();
            producoes.Add(terminal.ToString());

            m_gramatica.Producao.Add(new Producao(var, producoes));
        }

        private char AddVariavel()
        {
            int varivavel = 'z';
            bool aux = true;
            while (aux)
            {
                if (m_gramatica.Producao.Select(s => s.Variavel).FirstOrDefault(f => f == varivavel) != '\0')
                {
                    aux = false;
                }
                else
                    varivavel--;
            }
            return Convert.ToChar(varivavel);
        }
    }
}
