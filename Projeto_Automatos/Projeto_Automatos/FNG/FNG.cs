﻿using Projeto_Automatos.model;
using Projeto_Automatos.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.FNG
{
    public class FNG
    {
        Gramatica Gramatica;

        public FNG(Gramatica gramatica)
        {
            Gramatica = gramatica;
        }

        public void VerificarRMenorIgualS()
        {
            foreach (var producao in Gramatica.Producao)
            {
                int variavel = producao.Variavel;
                //verificar producoes de uma variavel
                for (int i = 0; i < producao.Producoes.Count; i++)
                {
                    //se o primeiro produto for mais que a variavel
                    if(producao.Producoes[i][0] > variavel)
                    {
                        //troca a producao pela equivalente da variavel
                        List<string> NovaProducao = SubstituirVariavel(producao.Producoes[i][0],producao.Producoes[i]);
                        for (int j = 0; j < NovaProducao.Count; j++)
                        {
                            //substituindo a producao atual pela a nova
                            if (j == 0)
                                producao.Producoes[i] = NovaProducao[j];
                            else
                                producao.Producoes.Add(NovaProducao[j]);
                        }
                    }
                }
            }
            VerificarRecursao();
        }

        private void VerificarRecursao()
        {
            foreach (var producao in Gramatica.Producao)
            {
                char novaVariavel = '\0';
                for (int i = 0; i < producao.Producoes.Count; i++)
                {
                    //verificar se existe recursao
                    if(producao.Producoes[i][0] == producao.Variavel)
                    {
                        //criacao de nova variavel
                        novaVariavel = RemoverRecursao(producao.Variavel,producao.Producoes[i]);                         
                    }
                }
                //se adicionado nova variavel corrigi-se a producao
                if (novaVariavel != '\0')
                   producao.Producoes = CorrigirProducaoNovaVariavel(novaVariavel, producao.Variavel);
            }
            VerificaInicialIgualTerminal();
        }

        private void VerificaInicialIgualTerminal()
        {
            foreach (var producao in Gramatica.Producao)
            {
                //caso o primeiro seja uma variavel alterarei o bool para true e salvarei o index
                bool troca = false;
                int index =0;
                for (int i = 0; i < producao.Producoes.Count; i++)
                {                    
                    if(char.IsUpper(producao.Producoes[i][0]))
                    {
                        troca = true;
                        index = i;
                    }
                }
                //caso seja necessario remover a variavel e adicionar sua respectiva producao
                if (troca)
                  producao.Producoes = TrocaVariavelPorProducao(producao, index);

            }
        }

        private List<string> TrocaVariavelPorProducao(Producao producao, int index)
        {
            //pegando a variavel a ser removivel
            char variavel = producao.Producoes[index][0];
            //pegando producao da variavel
            List<string> addproducao = GetProducao(variavel);
            //removendo a variavel
            producao.Producoes[index].Remove(0);
            //tamanho da lista
            int count = producao.Producoes.Count;
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < addproducao.Count; j++)
                {
                    //se não existir producao igual adiciona
                    if (producao.Producoes[i] != addproducao[j])
                        producao.Producoes.Add(addproducao[j]);
                }
            }
            //retorna a nova producao
            return producao.Producoes;
        }


        public void VerificarTerminalVariavel()
        {
            
            foreach (var producao in Gramatica.Producao)
            {
                for (int i = 0; i < producao.Producoes.Count; i++)
                {
                    if (producao.Producoes[i].Length > 1)
                    {
                        char[] aux = producao.Producoes[i].ToCharArray();
                        bool troca = false;
                        for (int j = 1; j < producao.Producoes[i].Length; j++)
                        {
                            if(char.IsLower(producao.Producoes[i][j]))
                            {                              
                              char novaVariavel = RemoveTerminalAddNovaVariavel(producao.Producoes[i][j]);
                              aux[j] = novaVariavel;
                              troca = true;
                            }
                        }
                        if(troca)
                        producao.Producoes[i] = aux.ToString();
                    }
                }
            }
        }

        private char RemoveTerminalAddNovaVariavel(char p)
        {
          char NovaVariavel = AddVariavel();
          List<string> novaLista = new List<string>();
          novaLista.Add(p.ToString());
          Gramatica.Producao.Add(new Producao(NovaVariavel, novaLista));
          return NovaVariavel;
        }

        private List<string> CorrigirProducaoNovaVariavel(char novaVariavel, char variavel)
        {
            //pegando producao atual
            var producao = GetProducao(variavel);
            //verificando tamanho da producao
            int count = producao.Count;
            for (int i = 0; i < count; i++)
            {
                //verificar para remover a recursao
                if(producao[i][0] == variavel)
                    producao[i].Remove(0);
                //adicionando a nova variavel a producao
                producao.Add(producao[i] + novaVariavel);
            }
            return producao;
        }

        private char RemoverRecursao(char p,string producao)
        {
            char novaVariavel = AddVariavel();
            producao.Remove(0);
            List<string> novaproducao = new List<string>();
            novaproducao.Add(producao);
            novaproducao.Add(producao+novaVariavel);
            Gramatica.Producao.Add(new Producao(novaVariavel,novaproducao));
            return novaVariavel;
        }

        private char AddVariavel()
        {
            int varivavel = 'z';
            bool aux = true;
            while(aux)
            {
                if (Gramatica.Producao.Select(s => s.Variavel).FirstOrDefault(f => f == varivavel) != '\0')
                {
                    aux = false;
                }
                else
                    varivavel--;
            }
            return Convert.ToChar(varivavel);
        }

        private List<string> SubstituirVariavel(char Variavel, string producao)
        {
            producao.Remove(0);
            List<string> producaoVariavel = GetProducao(Variavel);
            //adicionando a producao correta
            for (int i = 0; i < producaoVariavel.Count; i++)
            {
                producaoVariavel[i] += producao;
            }
            return producaoVariavel;
        }

        private List<string> GetProducao(char variavel)
        {
            foreach (var producao in Gramatica.Producao)
            {
                if (variavel == producao.Variavel)
                    return producao.Producoes;
            }
            return null;
        }

    }
}
