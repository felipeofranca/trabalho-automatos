﻿using Projeto_Automatos.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.simplificacao
{
    public class Etapa1
    {
        Gramatica Gramatica;
        List<char> ProducoesVazia = new List<char>();
        public Etapa1(Gramatica gramatica)
        {
            Gramatica = gramatica;
        }

        public void ProcurarVazio()
        {
            char producaoVazia = 'a';
            bool TemVazio = false;
            foreach (var producao in Gramatica.Producao)
            {
                var vazio = producao.Producoes.Find(w => w.Contains('#'));
                if (vazio != null)
                {
                    TemVazio = true;
                    producao.Producoes.Remove("#");
                    producaoVazia = producao.Variavel;
                    ProducoesVazia.Add(producaoVazia);
                    break;
                }
            }

            if(TemVazio)
            {
                ProcurarVazioIndireto(producaoVazia);
                ProducaoVaziaEtapa2();
            }
            //ToDo: Etapa 3
        }

        private void ProducaoVaziaEtapa2()
        {
            for (int i = 0; i < ProducoesVazia.Count; i++)
            {
                foreach (var producoes in Gramatica.Producao)
                {
                    string prod = producoes.Producoes.FirstOrDefault(f => f.Contains(ProducoesVazia[i]) && f.Length > 1);
                    if (prod != null)
                    {
                        int position = 0;
                        for (int j = 0; j < prod.Length; j++)
                        {                            
                            if(prod[j] == ProducoesVazia[i])
                            {
                                position = j;
                            }
                        }
                        producoes.Producoes.Add(prod.Remove(position));
                    }
                }
            }
        }


        private void ProcurarVazioIndireto(char producaoVazia)
        {
            foreach (var producao in Gramatica.Producao)
            {
                if (string.IsNullOrEmpty(producao.Producoes.Where(w => w == producaoVazia.ToString()).ToString()))
                {
                    ProducoesVazia.Add(producao.Variavel);
                    ProcurarVazioIndireto(producao.Variavel);
                    return;
                }
            }
        }
    }

}
