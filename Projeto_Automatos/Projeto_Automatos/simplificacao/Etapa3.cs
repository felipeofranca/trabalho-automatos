﻿using Projeto_Automatos.model;
using Projeto_Automatos.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.simplificacao
{
    public class Etapa3
    {
        private Gramatica m_gramatica;

        public Etapa3(Gramatica gramatica)
        {
            m_gramatica = gramatica;
        }

        public Gramatica RetirarInuteisTotal()
        {
            VerificaAcessoTerminais();
            RemoverSimbolosInuteis();

            return m_gramatica;
        }

        public void VerificaAcessoTerminais()
        {
            List<int> aux = new List<int>();

            for (int i = 0; i < m_gramatica.Producao.Count; i++)
            {
                for (int j = 0; j < m_gramatica.Producao[i].Producoes.Count; j++)
                {
                    bool temTerminal = false;

                    foreach (var c in m_gramatica.Producao[i].Producoes[j])
                    {
                        //Se for minúscula significa que é terminal (pois já foi verificado anteriormente)
                        if (char.IsLower(c))
                        {
                            temTerminal = true;
                        }
                    }

                    if (!temTerminal)
                        aux.Add(i);
                }
            }

            //Remover todas as referências dessa produção
            foreach (int item in aux)
            {
                m_gramatica.Producao.RemoveAt(item);
            }

            VerificaTodosAcessos(m_gramatica.Producao[0].Producoes, m_gramatica.Producao[0].Variavel);
        }

        private List<char> m_variaveis = new List<char>();
        private List<char> m_terminais = new List<char>();

        public void VerificaTodosAcessos(List<string> prod, char initial = ' ')
        {
            if (initial != ' ')
                m_variaveis.Add(initial);

            for (int i = 0; i < prod.Count; i++)
            {
                for (int j = 0; j < prod[i].Length; j++)
                {
                    if (char.IsLower(prod[i][j]) && !m_terminais.Contains(prod[i][j]))
                        m_terminais.Add(prod[i][j]);

                    if (char.IsUpper(prod[i][j]) && !m_variaveis.Contains(prod[i][j]))
                    {
                        m_variaveis.Add(prod[i][j]);
                        VerificaTodosAcessos(GetProducao(prod[i][j]));
                    }
                }
            }
        }

        private void RemoverSimbolosInuteis()
        {
            RemoverVariaves();

            RemoverTerminais();
        }

        private void RemoverVariaves()
        {
            //Pegar as variaveis da gramatica
            List<char> varGramatica = m_gramatica.Producao.Select(s => s.Variavel).ToList();

            //Variaveis que devem ser removidos
            List<int> variaveisRemover = new List<int>();

            for (int i = 0; i < varGramatica.Count; i++)
            {
                bool remove = true;

                for (int j = 0; j < m_variaveis.Count; j++)
                {
                    if (varGramatica[i] == m_variaveis[j])
                        remove = false;
                }

                if (remove)
                    variaveisRemover.Add(i);
            }

            foreach (int i in variaveisRemover)
                m_gramatica.Producao.RemoveAt(i);
        }

        private void RemoverTerminais()
        {
            //Terminais que devem ser removidos
            List<int> terminaisRemover = new List<int>();

            for (int i = 0; i < m_gramatica.Terminais.Count; i++)
            {
                bool remove = true;

                for (int j = 0; j < m_terminais.Count; j++)
                {
                    if (m_gramatica.Terminais[i] == m_terminais[j].ToString())
                        remove = false;
                }

                if (remove)
                    terminaisRemover.Add(i);
            }

            foreach (var item in terminaisRemover)
                m_gramatica.Terminais.RemoveAt(item);
        }

        private List<string> GetProducao(char variavel)
        {
            foreach (var producao in m_gramatica.Producao)
            {
                if (variavel == producao.Variavel)
                    return producao.Producoes;
            }
            return null;
        }
    }
}
