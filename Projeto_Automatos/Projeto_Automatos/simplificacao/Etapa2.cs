﻿using Projeto_Automatos.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.simplificacao
{
    public class Etapa2
    {
        Gramatica Gramatica;
        List<Fecho> Fechos = new List<Fecho>();
        List<char> Variaveis = new List<char>();

        public Etapa2(Gramatica gramatica)
        {
            Gramatica = gramatica;
        }

        public void ProcurarFechos()
        {
            Variaveis = Gramatica.Producao.Select(s => s.Variavel).ToList();
            foreach (var producao in Gramatica.Producao)
            {
                for (int i = 0; i < Variaveis.Count; i++)
                {
                    var prod = producao.Producoes.FirstOrDefault(f => f == Variaveis[i].ToString());                    
                   if (prod != null)
                   {
                       
                       Fechos.Add(new Fecho(producao.Variavel,Variaveis[i] , GetProducao(Variaveis[i])));
                   }                       
                }                
            }
            RemoverProducaoUnitaria();
            VerificarFechos();
        }

        private List<string> GetProducao(char variavel)
        {
            foreach (var producao in Gramatica.Producao)
            {
                if (variavel == producao.Variavel)
                    return producao.Producoes;
            }
            return null;
        }

        private void VerificarFechos()
        {
            foreach (var producao in Gramatica.Producao)
            {
                foreach (var fecho in Fechos)
                {
                    if(producao.Variavel == fecho.FechoVariavel)
                    {
                       producao.Producoes = AdicionarConteudoFecho(producao.Producoes, fecho.ProducaoProduto);
                    }
                }                
            }
        }

        private List<string> AdicionarConteudoFecho(List<string> producoes, List<string> produtos)
        {
            List<string> aux = producoes;
            for (int i = 0; i < producoes.Count; i++)
            {
                string addproducao = string.Empty;
                for (int j = 0; j < produtos.Count; j++)
                {
                    if (producoes[i] != produtos[j])
                    {
                        addproducao = produtos[j];
                        break;
                    }                        
                }
                if (!String.IsNullOrEmpty(addproducao))
                    aux.Add(addproducao);
            }
            return aux;
        }

        private void RemoverProducaoUnitaria()
        {
            foreach (var producao in Gramatica.Producao)
            {
                for (int i = 0; i < producao.Producoes.Count; i++)
                {
                    if(producao.Producoes[i].Length == 1)
                    {
                        char unitario = Convert.ToChar(producao.Producoes[i]);
                        if(Variaveis.Contains(unitario))
                        {
                            producao.Producoes.RemoveAt(i);
                        }
                    }
                }
            }
        }
    }
}
