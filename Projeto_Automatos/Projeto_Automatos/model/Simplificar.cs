﻿using Projeto_Automatos.model;
using Projeto_Automatos.simplificacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.MODEL
{
    public class Simplificar
    {
        public Gramatica Gramatica;
        public Simplificar(Gramatica gramatica)
        {
            Gramatica = gramatica;
            Etapa1 Etapa1 = new Etapa1(gramatica);
            Etapa1.ProcurarVazio();

            Etapa2 etapa2 = new Etapa2(gramatica);
            etapa2.ProcurarFechos();

            Etapa3 etapa3 = new Etapa3(gramatica);
            Gramatica = etapa3.RetirarInuteisTotal();
         }
    }       
}
