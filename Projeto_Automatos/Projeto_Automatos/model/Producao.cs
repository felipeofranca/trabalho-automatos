﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.MODEL
{
    public class Producao
    {
        public char Variavel { get; set; }
        public List<string> Producoes { get; set; }

        public Producao(char variavel, List<string> producoes)
        {
            Variavel = variavel;
            Producoes = producoes;
        }
    }
}
