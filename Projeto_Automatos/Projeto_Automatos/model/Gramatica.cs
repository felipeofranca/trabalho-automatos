﻿using Projeto_Automatos.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.model
{
    public class Gramatica
    {
        public List<string> Terminais { get; set; }
        public List<Producao> Producao { get; set; }

        public Gramatica(List<string> terminais, List<Producao> producao)
        {
            Terminais = terminais;
            Producao = producao;
        }
    }
}
