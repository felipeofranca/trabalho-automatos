﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_Automatos.model
{
    public class Fecho
    {
        //Fecho selecionado
        public char FechoVariavel { get; set; }
        //Variável atingido pelo fecho
        public char Produto { get; set; }
        //Produção da variável atingida pelo fecho
        public List<string> ProducaoProduto { get; set; }

        public Fecho(char fecho, char produto,List<string> producaoproduto)
        {
            FechoVariavel = fecho;
            Produto = produto;
            ProducaoProduto = producaoproduto;
        }
    }
}
